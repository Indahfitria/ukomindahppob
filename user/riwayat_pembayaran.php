<?php 
  include 'koneksi.php';
  session_start();
  if (!isset($_SESSION['username'])) {
    header('location:../login.php');
  }
  else{
    $query_pelanggan = mysqli_query($koneksi, "SELECT * FROM pelanggan where username='$_SESSION[username]'");
    $pelanggan = mysqli_fetch_array($query_pelanggan);
    
  }
  ?>
<!doctype html>
<html lang="en"><head>
    <meta charset="utf-8">
    <title>APLIKASI PEMBAYARAN LISTRIK</title>
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="lib/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="lib/font-awesome/css/font-awesome.css">
   <script src="lib/jquery-1.11.1.min.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="stylesheets/theme.css">
    <link rel="stylesheet" type="text/css" href="stylesheets/premium.css">

</head>
<body class=" theme-blue">
    <script type="text/javascript">
        $(function() {
            var match = document.cookie.match(new RegExp('color=([^;]+)'));
            if(match) var color = match[1];
            if(color) {
                $('body').removeClass(function (index, css) {
                    return (css.match (/\btheme-\S+/g) || []).join(' ')
                })
                $('body').addClass('theme-' + color);
            }

            $('[data-popover="true"]').popover({html: true});
            
        });
    </script>
    <style type="text/css">
        #line-chart {
            height:300px;
            width:800px;
            margin: 0px auto;
            margin-top: 1em;
        }
        .navbar-default .navbar-brand, .navbar-default .navbar-brand:hover { 
            color: #fff;
        }
    </style>

    <script type="text/javascript">
        $(function() {
            var uls = $('.sidebar-nav > ul > *').clone();
            uls.addClass('visible-xs');
            $('#main-menu').append(uls.clone());
        });
    </script>
    <link rel="shortcut icon" href="../assets/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">

    <div class="navbar navbar-default" role="navigation">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="" href="index.html"><span class="navbar-brand"> I-PLN</span></a></div>

        <div class="navbar-collapse collapse" style="height: 1px;">
          <ul id="main-menu" class="nav navbar-nav navbar-right">
            <li class="dropdown hidden-xs">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <span class="glyphicon glyphicon-user padding-right-small" style="position:relative;top: 3px;"></span> <?php echo $pelanggan['nama_pelanggan'];?>
                    <i class="fa fa-caret-down"></i>
                </a>

              <ul class="dropdown-menu">
                <li><a href="./">My Account</a></li>
                
                
                <li class="divider"></li>
                <li><a tabindex="-1" href="../logout.php">Logout</a></li>
              </ul>
            </li>
          </ul>

        </div>
      </div>
    </div>
    

    <div class="sidebar-nav">
    <ul><br>
        <img src="7.png" style="width: 65%; margin-left: 15% "  class="user-image img-responsive"/>
        
        <br>
    <li><a href="#" data-target=".dashboard-menu" class="nav-header" data-toggle="collapse">
<li><a href="index.php" class="nav-header"><i class="fa fa-home"></i> Dashboard</a></li>
            <li><a href="#" data-target=".legal-menu" class="nav-header collapsed" data-toggle="collapse"><i class="fa fa-fw fa-legal"></i> Riwayat<i class="fa fa-collapse"></i></a></li>
        <li><ul class="legal-menu nav nav-list collapse">
            <li ><a href="riwayat_pembayaran.php"><span class="fa fa-caret-right"></span> Riwayat Pembayaran</a></li>
            <li ><a href="riwayat_tagihan.php"><span class="fa fa-caret-right"></span> Riwayat Tagihan</a></li>
            <li ><a href="riwayat_topup.php"><span class="fa fa-caret-right"></span> Riwayat Top-up</a></li>
    </ul></li>
            </ul>
    </div>

    <div class="content">
       <div class="header">
            

            <h3 class="page-title">Saldo  :  RP. <?php echo $pelanggan['saldo']; ?> </h3>
                    <ul class="breadcrumb">
            <a href="saldo.php" class="btn btn-primary" style="margin-left: 90%;" >Saldo</a> 
        </ul>

        </div>
        

        <div class="main-content">
            
 <div class="row">
        <div class="col-md-12">
            <div class="panel-body">
            <div class="table-responsive">
              <table class="table table-striped table-bordered table-hover" id="example">
  <thead>
    <tr>
      <th>No</th>
      <th>Tanggal Pembayaran</th>
      <th>Bulan Bayar</th>
      <th>Total Bayar</th>
      <th>Aksi</th>
    </tr>
  </thead>
  <tbody>
    <?php 
            $no = 1;
      $pilih = mysqli_query ($koneksi,"SELECT * FROM pembayaran where id_pelanggan='$_SESSION[id_pelanggan]'");
      while($data=mysqli_fetch_array($pilih)){
      ?>
      <tr>
        <td><?php echo $no++; ?></td>
        <td><?php echo $data['tanggal_pembayaran']; ?></td>
        <td>
          <?php 
            switch ($data['bulan_bayar'])
            {
              case "1";
                $bulan_bayar = "Januari";
              break;
              case "2";
                $bulan_bayar = "Februari";
              break;
              case "3";
                $bulan_bayar = "Maret";
              break;
              case "4";
                $bulan_bayar = "April";
              break;
              case "5";
                $bulan_bayar = "Mei";
              break;
              case "6";
                $bulan_bayar = "Juni";
              break;
              case "7";
                $bulan_bayar = "Juli";
              break;
              case "8";
                $bulan_bayar = "Agustus";
              break;
              case "9";
                $bulan_bayar = "September";
              break;
              case "10";
                $bulan_bayar = "Oktober";
              break;
              case "11";
                $bulan_bayar = "November";
              break;
              case "12";
                $bulan_bayar = "Desember";
              break;
            } 
            echo $bulan_bayar;
          ?>
        </td>
        <td><?php echo $data['total_bayar']; ?></td>
        <td>
          <a href="detail.php?id=<?php echo $data['id_pembayaran'];?>" button  class="btn btn-primary">detail</button>
        </td>
          </tr>
        <?php  
          }
        ?>
      </tbody>
    </table>
              </div>
            </div>
</div>
   </div>
    </div>


            <footer>
                <hr>

                <!-- Purchase a site license to remove this link from the footer: http://www.portnine.com/bootstrap-themes -->
                <p class="pull-right"> <a href="#"></a>  <a href="#"></a></p>
                <p>© 2019<a href="#" target="_blank"></a></p>
            </footer>
        </div>
    </div>


    <script src="lib/bootstrap/js/bootstrap.js"></script>
    <script type="text/javascript">
        $("[rel=tooltip]").tooltip();
        $(function() {
            $('.demo-cancel-click').click(function(){return false;});
        });
    </script>
    
  
</body></html>
