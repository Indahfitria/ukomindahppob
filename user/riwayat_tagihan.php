<?php 
  include 'koneksi.php';
 session_start();
  if (!isset($_SESSION['username'])) {
    header('location:../login.php');
  }
  else{
    $query_pelanggan = mysqli_query($koneksi, "SELECT * FROM pelanggan where username='$_SESSION[username]'");
    $pelanggan = mysqli_fetch_array($query_pelanggan);
  }
  ?>

<!doctype html>
<html lang="en"><head>
    <meta charset="utf-8">
    <title>APLIKASI PEMBAYARAN LISTRIK</title>
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="lib/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="lib/font-awesome/css/font-awesome.css">
   <script src="lib/jquery-1.11.1.min.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="stylesheets/theme.css">
    <link rel="stylesheet" type="text/css" href="stylesheets/premium.css">

</head>
<body class=" theme-blue">
    <script type="text/javascript">
        $(function() {
            var match = document.cookie.match(new RegExp('color=([^;]+)'));
            if(match) var color = match[1];
            if(color) {
                $('body').removeClass(function (index, css) {
                    return (css.match (/\btheme-\S+/g) || []).join(' ')
                })
                $('body').addClass('theme-' + color);
            }

            $('[data-popover="true"]').popover({html: true});
            
        });
    </script>
    <style type="text/css">
        #line-chart {
            height:300px;
            width:800px;
            margin: 0px auto;
            margin-top: 1em;
        }
        .navbar-default .navbar-brand, .navbar-default .navbar-brand:hover { 
            color: #fff;
        }
    </style>

    <script type="text/javascript">
        $(function() {
            var uls = $('.sidebar-nav > ul > *').clone();
            uls.addClass('visible-xs');
            $('#main-menu').append(uls.clone());
        });
    </script>
    <link rel="shortcut icon" href="../assets/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">

    <div class="navbar navbar-default" role="navigation">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="" href="#"><span class="navbar-brand"> I-PLN</span></a></div>

        <div class="navbar-collapse collapse" style="height: 1px;">
          <ul id="main-menu" class="nav navbar-nav navbar-right">
            <li class="dropdown hidden-xs">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <span class="glyphicon glyphicon-user padding-right-small" style="position:relative;top: 3px;"></span>  <?php echo $pelanggan['nama_pelanggan'];?>
                    <i class="fa fa-caret-down"></i>
                </a>

              <ul class="dropdown-menu">
                <li><a href="#">My Account</a></li>
                
                
                <li class="divider"></li>
                <li><a tabindex="-1" href="../logout.php">Logout</a></li>
              </ul>
            </li>
          </ul>

        </div>
      </div>
    </div>
    <?php
      include "koneksi.php";
$today = date("Ymd"); //untuk mengambil tahun, tanggal dan tahun hari ini
$tanggal_daftar = date("d/m/Y");
//cari id terakhir di tanggal hari ini
$query1 = "SELECT max(id_tagihan) as maxID FROM tagihan WHERE id_tagihan LIKE '$today%'";
$hasil = mysqli_query($koneksi, $query1);
$data = mysqli_fetch_array($hasil);
$idMax = $data['maxID'];

//setelah membaca id terakhir , lanjut mencari nomor urut id dari id terakhir
$NoUrut = (int) substr($idMax, 8, 3);
$NoUrut++; //nomor urut +1

//setelah ketemu id terakhir lanjut membuat id baru dengan format sbb:
$NewID = $today .sprintf('%03s',$NoUrut);
//$today nanti jadinya misal 20160526 .sprintf('%03s', $NoUrut) urutan id di tanggal hari ini


?>
    <div class="sidebar-nav">
    <ul><br>
        <img src="7.png" style="width: 65%; margin-left: 15% "  class="user-image img-responsive"/>
        
        <br>
    <li><a href="#" data-target=".dashboard-menu" class="nav-header" data-toggle="collapse">
<li><a href="index.php" class="nav-header"><i class="fa fa-home"></i> Dashboard</a></li>
            <li><a href="#" data-target=".legal-menu" class="nav-header collapsed" data-toggle="collapse"><i class="fa fa-fw fa-legal"></i> Riwayat<i class="fa fa-collapse"></i></a></li>
        <li><ul class="legal-menu nav nav-list collapse">
            <li ><a href="riwayat_pembayaran.php"><span class="fa fa-caret-right"></span> Riwayat Pembayaran</a></li>
            <li ><a href="riwayat_tagihan.php"><span class="fa fa-caret-right"></span> Riwayat Tagihan</a></li>
            <li ><a href="riwayat_topup.php"><span class="fa fa-caret-right"></span> Riwayat Top-up</a></li>
    </ul></li>
            </ul>
    </div>
    <?php
include "koneksi.php";

$today = date("Ymd");
$tanggal_daftar = date("d/m/y");
$query1 = "SELECT max(id_tagihan) as maxID FROM tagihan WHERE id_tagihan LIKE '$today%'";
$hasil= mysqli_query($koneksi,$query1);
$data= mysqli_fetch_array($hasil);
$idMax= $data['maxID'];

$NoUrut = (int) substr($idMax, 8, 3);
$NoUrut++;

$NewID = $today .sprintf('%03s',$NoUrut);

$id_pelanggan =$_SESSION['id_pelanggan'];
$query_penggunaan = mysqli_query($koneksi,"SELECT * FROM penggunaan WHERE id_pelanggan='$id_pelanggan' order by bulan desc");
$penggunaan = mysqli_fetch_array($query_penggunaan);
$id_penggunaan = $penggunaan['id_penggunaan'];

$query_pelanggan = mysqli_query($koneksi,"SELECT * FROM pelanggan WHERE id_pelanggan='$id_pelanggan'");
$pelanggan = mysqli_fetch_array($query_pelanggan);
$nama_pelanggan =$pelanggan['nama_pelanggan'];
$id_tarif =$pelanggan['id_tarif'];


$query_tarif = mysqli_query($koneksi,"SELECT * FROM tarif WHERE id_tarif='$id_tarif'");
$tarif = mysqli_fetch_array($query_tarif);
$tarifdaya =$tarif['daya'];
$tarifperkwh =$tarif['tarifperkwh'];

$bulan_tagihan = $penggunaan['bulan'];
$tahun_tagihan = $penggunaan['tahun'];
$jumlah_meter_penggunaan =$penggunaan['meter_akhir']-$penggunaan['meter_awal'];
$jumlah_tagihan = $jumlah_meter_penggunaan * $tarifperkwh; 
$tahun_cek = date("Y");
$bulan_cek = date('n');
$tanggal_denda = date('d');
if ($tanggal_denda > 20) {
   $jumlah_denda = 5000;
 } else{
  $jumlah_denda = 0;
 }
 $query_tagihan_belum_bayar = mysqli_query($koneksi,"SELECT * FROM tagihan WHERE id_pelanggan='$id_pelanggan' and tahun=$tahun_cek and status='Belum Dibayar'");
 $cek_tagihan_belum_dibayar = mysqli_num_rows($query_tagihan_belum_bayar);
 if($cek_tagihan_belum_dibayar > 0){
  echo "<script>window.alert('Masih ada tagihan yang belum lunas')</script>";
  $tagihan = mysqli_fetch_array($query_tagihan_belum_bayar);
  $status = $tagihan['status'];
  $id_tagihan = $tagihan['id_tagihan'];
 }
 
?>

    <div class="content">
       <div class="header">
            

            <h3 class="page-title">Saldo  :  RP. <?php echo $pelanggan['saldo']; ?> </h3>
                    <ul class="breadcrumb">
            <a href="saldo.php" class="btn btn-primary" style="margin-left: 90%;" >Saldo</a> 
        </ul>

        </div>
        

        <div class="main-content">
            
 <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                        <div class="panel-body">
         <h4 align="center" >Tagihan</h4>
          <br>
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="example">
            <thead>
              <tr>
                <th>ID Tagihan</th>
                <th>Bulan</th>
                <th>Tahun</th>
                <th>Jumlah Penggunaan</th>
                <th>Status</th>
              </tr>
            </thead>
            <tbody>
             
              <?php

              include 'koneksi.php';
              $query_tagihan = mysqli_query($koneksi, "SELECT * FROM tagihan WHERE id_pelanggan='$_SESSION[id_pelanggan]'");
              while ($tagihan=mysqli_fetch_array($query_tagihan)) {
                $id_pelanggan = $_SESSION['id_pelanggan'];
                $query_penggunaan = mysqli_query($koneksi, "SELECT * FROM penggunaan WHERE id_pelanggan='$id_pelanggan' and bulan='$tagihan[bulan]'");
                $penggunaan = mysqli_fetch_array($query_penggunaan);
                $id_penggunaan = $penggunaan['id_penggunaan'];

//Penggambilan data di tabel pelanggan
                $query_pelanggan = mysqli_query($koneksi, "SELECT * FROM pelanggan WHERE id_pelanggan='$id_pelanggan'");
                $pelanggan = mysqli_fetch_array($query_pelanggan);
                $nama_pelanggan = $pelanggan['nama_pelanggan'];
                $id_tarif = $pelanggan['id_tarif'];

//pengambilan data di tabel tarif berdasarkan id tarif pelanggan
                $query_tarif = mysqli_query($koneksi, "SELECT * FROM tarif WHERE id_tarif='$id_tarif'");
                $tarif = mysqli_fetch_array($query_tarif);
                $tarifperkwh = $tarif['tarifperkwh'];

                $bulan_tagihan = $penggunaan['bulan'];
                $tahun_tagihan = $penggunaan['tahun'];
                $jumlah_meter_penggunaan = $penggunaan['meter_akhir'] - $penggunaan['meter_awal'];
                $jumlah_tagihan = $jumlah_meter_penggunaan * $tarifperkwh;
                $tahun_cek = date("Y");
                $bulan_cek = date('n');
                $tanggal_denda = date('d');
                if ($tanggal_denda > 20){
                  $jumlah_denda = 5000;
                }else {
                  $jumlah_denda = 0;
                } ?>
                <tr>
                  <td><?php echo $tagihan['id_tagihan']; ?></td>
                  <td>
                    <?php
                    $id_tagihan = $tagihan['id_tagihan'];
                    switch ($tagihan['bulan']) {
                      case "1";
                      $bulan = "Januari";
                      break;
                      case "2";
                      $bulan = "Februari";
                      break;
                      case "3";
                      $bulan = "Maret";
                      break;
                      case "4";
                      $bulan = "April";
                      break;
                      case "5";
                      $bulan = "Mei";
                      break;
                      case "6";
                      $bulan = "Juni";
                      break;
                      case "7";
                      $bulan = "Juli";
                      break;
                      case "8";
                      $bulan = "Agustus";
                      break;
                      case "9";
                      $bulan = "September";
                      break;
                      case "10";
                      $bulan = "Oktober";
                      break;
                      case "11";
                      $bulan = "November";
                      break;
                      case "12";
                      $bulan = "Desember";
                      break;
                    }
                    echo $bulan; ?>
                  </td>
                  <td><?php echo $tagihan['tahun']; ?></td>
                  <td><?php echo $tagihan['jumlah_meter']; ?></td>
                  <td><?php 
                  if ($tagihan['status'] == "Lunas") {
                    echo "<button class='btn btn-success btn-sm'>".$tagihan['status']."</button>";
                  }
                  else {
                    echo "<button data-toggle='modal' data-target='#bayar$id_tagihan'class='btn btn-danger btn-sm'>".$tagihan['status']."</button>";
                  }
                  ?></td>
                </tr>

                <!-- Modal -->
                <div class="modal fade" id="bayar<?php echo $id_tagihan;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Riwayat Tagihan yang belum Dibayar</h4>
                      </div>
                      <div class="modal-body">
                        <form action="proses_pembayaran.php?p=<?php echo $id_tagihan; ?>" method="POST">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Nama Pelanggan</label>
                            <div class="col-sm-9">
                              <input type="text" name="nama_pelanggan" class="form-control" placeholder="Nama Pelanggan" value="<?php echo $pelanggan['nama_pelanggan']; ?>" readonly>
                            </div>
                          </div>
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Nomor KWH</label>
                            <div class="col-sm-9">
                              <input type="text" name="nomor_kwh" class="form-control" value="<?php echo $pelanggan['nomor_kwh']; ?>" placeholder="Nomor kwh" readonly>
                            </div>
                          </div>
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Daya (Watt)</label>
                            <div class="col-sm-9">
                              <input type="number" name="daya" class="form-control" placeholder="Daya" value="<?php echo $tarif['daya']; ?>" readonly>
                            </div>
                          </div>
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Tarif Per-Kwh</label>
                            <div class="col-sm-9">
                              <input type="number" name="tarifperkwh" class="form-control" placeholder="Bulan Bayar" value="<?php echo $tarif['tarifperkwh']; ?>" readonly>
                            </div>
                          </div>
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Jumlah Penggunaan</label>
                            <div class="col-sm-9">
                              <input type="number" name="jumlah_penggunaan" class="form-control" placeholder="Jumlah Bayar" value="<?php $jumlah = $penggunaan['meter_akhir'] - $penggunaan['meter_awal']; echo $jumlah; ?>" readonly>
                            </div>
                          </div>
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Jumlah Tagihan</label>
                            <div class="col-sm-9">
                              <input type="text" name="jumlah_tagihan" class="form-control"  placeholder="Total Bayar" value="<?php echo $jumlah_tagihan; ?>" readonly>
                            </div>
                          </div>
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Biaya Admin</label>
                            <div class="col-sm-9">
                              <input type="text" name="biaya_admin" class="form-control"  placeholder="Total Bayar" value="2500" readonly>
                            </div>
                          </div>
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Denda</label>
                            <div class="col-sm-9">
                              <input type="text" name="denda" class="form-control"  placeholder="Total Bayar" value="<?php echo $jumlah_denda; ?>" readonly>
                            </div>
                          </div>
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Total Bayar</label>
                            <div class="col-sm-9">
                              <input type="text" name="denda" class="form-control"  placeholder="Total Bayar" value="<?php $jumlah_bayar=$jumlah_tagihan + 2500 + $jumlah_denda; echo $jumlah_bayar; ?>" readonly>
                            </div>
                          </div>
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Status</label>
                            <div class="col-sm-9">
                              <input type="text" name="status" class="form-control"  placeholder="Total Bayar" value="<?php echo $tagihan['status']; ?>" readonly>
                            </div>
                          </div>
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Metode Pembayaran</label>
                            <div class="col-sm-9">
                              <select class="form-control" name="metode_pembayaran">
                                <option value="saldo">Saldo</option>
                                <option value="BRI">Bank BRI</option>
                                <option value="Mandiri">Bank Mandiri</option>
                                <option value="BNI">Bank BNI</option>
                              </select>
                            </div>
                          </div>
                        </div>
                        <div class="modal-footer">
                          <input type="submit" class="btn btn-danger" name="bayar" value="Bayar">
                          <button type="button" class="btn btn-info" data-dismiss="modal">Close</button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>

                <?php } ?>


            </tbody>
          </table>
                            </div>
        </div>
      </div>
</div>
</div>
   </div>
    


            <footer>
                <hr>

                <!-- Purchase a site license to remove this link from the footer: http://www.portnine.com/bootstrap-themes -->
                <p class="pull-right"> <a href="#"></a>  <a href="#"></a></p>
                <p>© 2019<a href="#" target="_blank"></a></p>
            </footer>
        </div>
    </div>


    <script src="lib/bootstrap/js/bootstrap.js"></script>
    <script type="text/javascript">
        $("[rel=tooltip]").tooltip();
        $(function() {
            $('.demo-cancel-click').click(function(){return false;});
        });
    </script>
    
  
</body></html>
