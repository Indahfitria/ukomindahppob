<?php 
  include 'koneksi.php';
  session_start();
  if (!isset($_SESSION['username'])) {
    header('location:../login.php');
  }
  else{
    $query_pelanggan = mysqli_query($koneksi, "SELECT * FROM pelanggan where username='$_SESSION[username]'");
    $pelanggan = mysqli_fetch_array($query_pelanggan);
    
  }
  ?>
<!doctype html>
<html lang="en"><head>
    <meta charset="utf-8">
    <title>APLIKASI PEMBAYARAN LISTRIK</title>
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="lib/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="lib/font-awesome/css/font-awesome.css">
   <script src="lib/jquery-1.11.1.min.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="stylesheets/theme.css">
    <link rel="stylesheet" type="text/css" href="stylesheets/premium.css">

</head>
<body class=" theme-blue">
    <script type="text/javascript">
        $(function() {
            var match = document.cookie.match(new RegExp('color=([^;]+)'));
            if(match) var color = match[1];
            if(color) {
                $('body').removeClass(function (index, css) {
                    return (css.match (/\btheme-\S+/g) || []).join(' ')
                })
                $('body').addClass('theme-' + color);
            }

            $('[data-popover="true"]').popover({html: true});
            
        });
    </script>
    <style type="text/css">
        #line-chart {
            height:300px;
            width:800px;
            margin: 0px auto;
            margin-top: 1em;
        }
        .navbar-default .navbar-brand, .navbar-default .navbar-brand:hover { 
            color: #fff;
        }
    </style>

    <script type="text/javascript">
        $(function() {
            var uls = $('.sidebar-nav > ul > *').clone();
            uls.addClass('visible-xs');
            $('#main-menu').append(uls.clone());
        });
    </script>
    <link rel="shortcut icon" href="../assets/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">

    <div class="navbar navbar-default" role="navigation">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="" href="index.html"><span class="navbar-brand"> I-PLN</span></a></div>

        <div class="navbar-collapse collapse" style="height: 1px;">
          <ul id="main-menu" class="nav navbar-nav navbar-right">
            <li class="dropdown hidden-xs">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <span class="glyphicon glyphicon-user padding-right-small" style="position:relative;top: 3px;"></span> <?php echo $pelanggan['nama_pelanggan'];?>
                    <i class="fa fa-caret-down"></i>
                </a>

              <ul class="dropdown-menu">
                <li><a href="./">My Account</a></li>
                
                
                <li class="divider"></li>
                <li><a tabindex="-1" href="../logout.php">Logout</a></li>
              </ul>
            </li>
          </ul>

        </div>
      </div>
    </div>
    

    <div class="sidebar-nav">
    <ul><br>
        <img src="7.png" style="width: 65%; margin-left: 15% "  class="user-image img-responsive"/>
        
        <br>
    <li><a href="#" data-target=".dashboard-menu" class="nav-header" data-toggle="collapse">
<li><a href="index.php" class="nav-header"><i class="fa fa-home"></i> Dashboard</a></li>
            <li><a href="#" data-target=".legal-menu" class="nav-header collapsed" data-toggle="collapse"><i class="fa fa-fw fa-legal"></i> Riwayat<i class="fa fa-collapse"></i></a></li>
        <li><ul class="legal-menu nav nav-list collapse">
            <li ><a href="riwayat_pembayaran.php"><span class="fa fa-caret-right"></span> Riwayat Pembayaran</a></li>
            <li ><a href="riwayat_tagihan.php"><span class="fa fa-caret-right"></span> Riwayat Tagihan</a></li>
            <li ><a href="riwayat_topup.php"><span class="fa fa-caret-right"></span> Riwayat Top-up</a></li>
    </ul></li>
            </ul>
    </div>

    <div class="content">
       

<div id="page-wrapper" >
      <div id="page-inner">
        <div class="row">
          <div class="col-md-12">  
           <h4 align="center">CETAK STRUK</h4>

         </div>
       </div>
       <!-- /. ROW  -->
       <hr />

        <?php
        $query_pelanggan = mysqli_query($koneksi, "SELECT * FROM pelanggan");
        $pelanggan = mysqli_fetch_array($query_pelanggan);

        $id_pelanggan = $_SESSION['id_pelanggan'];
        $query_pembayaran = mysqli_query($koneksi, "SELECT * FROM pembayaran WHERE id_pelanggan='$id_pelanggan'");
        $pembayaran = mysqli_fetch_array($query_pembayaran);{
        ?>

       <div class="row">
        <div class="col-md-12">
          <!-- Form Elements -->
          <div class="panel panel-default">
           
            <div class="panel-body">
              <div class="row">
                <div class="col-md-12">
                    <h3 align="center">STRUK PEMBAYARANLISTRIK PASCBAYAR</h3><br>
                </div>
                <div align="center">
                <table>
                  <tr>
                    <td>NOMOR METER</td>
                    <td><?php echo $pelanggan['nomor_kwh']; ?></td>
                    <td>TANGGAL PEMBELIAN</td>
                    <td><?php echo $pembayaran['tanggal_pembayaran']; ?></td>
                  </tr>
                  <tr>
                    <td>ID PELANGGAN</td>
                    <td><?php echo $pelanggan['id_pelanggan']; ?></td>
                    <td>jumlah pembayaran</td>
                    <td><?php echo $pembayaran['jumlah_bayar']; ?></td>
                  </tr>
                  <tr>
                    <td>NAMA PELANGGAN</td>
                    <td><?php echo $pelanggan['nama_pelanggan']; ?></td>
                    <td>BIAYA ADMIN</td>
                    <td><?php echo $pembayaran['biaya_admin']; ?></td>
                  </tr>
                  <?php
                    $id_tarif = $pelanggan['id_tarif'];
                    $query_tarif = mysqli_query($koneksi, "SELECT * FROM tarif WHERE id_tarif='$id_tarif'");
                    $tarif = mysqli_fetch_array($query_tarif);
                    {
                    ?>
                    <tr>
                    <td>TARIF/DAYA</td>
                    <td><?php echo $tarif['tarifperkwh']; ?>/<?php echo $tarif['daya']; ?></td>
                    <td>BIAYA DENDA</td>
                    <td><?php echo $pembayaran['denda']; ?></td>
                  </tr>
                  <tr>
                    <td>TOTAL PEMBAYARAN</td>
                    <td><?php echo $pembayaran['total_bayar']; ?></td>
                  </tr>
                  <?php
                    }
                    ?>
                </table>
            </div>
              </div>
            </div>
          </div>
        </div>
      </div>
<?php
}
?>
</div>
</div>
<script>
  window.load =print_d();
  function print_d(){
   window.print();
}
</script>

            
        </div>
    </div>


    <script src="lib/bootstrap/js/bootstrap.js"></script>
    <script type="text/javascript">
        $("[rel=tooltip]").tooltip();
        $(function() {
            $('.demo-cancel-click').click(function(){return false;});
        });
    </script>
    
  
</body></html>
