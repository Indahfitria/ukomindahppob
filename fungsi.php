<?php
	function ubahTanggal($tanggal){
 		$pisah = explode('/',$tanggal);
 		$array = array($pisah[2], $pisah[0], $pisah[1]);
 		$satukan = implode('-',$array);
 	
 		return $satukan;
	}

	function ambilBulan($param){
		$pisah = explode('-', $param);
		$array = array($pisah[2], $pisah[0], $pisah[1]);
		$hasil = $pisah[1];

		return $hasil;
	}

	function ambilTahun($param){
		$pisah = explode('-', $param);
		$array = array($pisah[2], $pisah[0], $pisah[1]);
		$hasil = $pisah[0];

		return $hasil;
	}

	function kodeAcak($panjang){
		 $karakter = '';
		 $karakter .= 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'; // karakter alfabet
		 $karakter .= '1234567890'; // karakter numerik
		 
		 $string = '';
		 for ($i=0; $i < $panjang; $i++) { 
		  $pos = rand(0, strlen($karakter)-1);
		  $string .= $karakter{$pos};
		 }

		 return $string;
	}
?>