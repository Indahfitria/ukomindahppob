﻿
<?php 
  include 'koneksi.php';
 session_start();
  if (!isset($_SESSION['username'])) {
    header('location:../login.php');
  }
  else{
    $query_admin = mysqli_query($koneksi, "SELECT * FROM admin where username='$_SESSION[username]'");
    $admin = mysqli_fetch_array($query_admin);
  }
  ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
      <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Aplikasi Pembayaran Lisrik</title>
    <link href="../assets/css/bootstrap.css" rel="stylesheet" />
    <link href="../assets/css/font-awesome.css" rel="stylesheet" />
    <link href="../assets/css/custom.css" rel="stylesheet" />
   <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
    <link href="../assets/js/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
</head>
<body>
    <div id="wrapper">
        <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand">I-PLN</a> 
            </div>
  <div style="color: white;
padding: 15px 50px 5px 50px;
float: right;
font-size: 16px;"><a href="../logout.php" class="btn btn-danger square-btn-adjust">Logout</a> </div>
        </nav>   
           <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">

				<li class="text-center">

                    <img src="../assets/img/7.png" class="user-image img-responsive"/>	
                    <h3 style="color: white;">Hallo Admin !!</h3>
                    <li>
                        <a href="#"><i class="fa fa-sitemap fa-2x"></i> Master Data<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="tarif.php">tarif</a>
                                <a href="index.php">Data Pengguna</a>
                            </li>
                           
                        </ul>
                      </li>  
                  <li  >
                    </li>
                    </li>

						 
                    <li>
                        <a href="#"><i class="fa fa-credit-card fa-2x"></i> Laporan <span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="riwayat_pembayaran.php">Riwayat Pembayaran</a>
                                <a href="riwayat_tagihan.php">Riwayat Tagihan</a>
                                <a href="top_up.php">Riwayat TOP-UP</a>
                            </li>
                           
                        </ul>
                      </li>
                    <li>
                        <a  href="verifikasi.php"><i class="fa fa-dashboard fa-2x"></i> Verifikasi</a>
                    </li>
                     
                    <li>
                     <a  href="backup.php"><i class="fa fa-download fa-2x"></i> Backup  </a>
                    </li>   				
                </ul>
               
            </div>
            
        </nav>  
        <!-- /. NAV SIDE  -->



        <div id="page-wrapper" >
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                     <h2 align="center">Data Pengguna</h2>  
                    </div>
                </div>
                 <!-- /. ROW  -->
                 <hr />
               <!-- Button trigger modal -->

<a href="print_pengguna.php" class="btn btn-primary">
  Print To Pdf</a>

<!-- Modal -->

<br>
<br>
            <div class="row">
                <div class="col-md-12">
                    <!-- Advanced Tables -->
                    <div class="panel panel-default">
                       
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>ID.Pelanggan</th>
                                            <th>Nama Pelanggan</th>
                                            <th>Nomor Kwh</th>
                                            <th>Alamat</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                     <?php
$no=1;
$tampil = mysqli_query($koneksi ," SELECT * from pelanggan order by id_pelanggan desc");
while($data = mysqli_fetch_array($tampil)){
  ?>

                                      <tr>
                                        <td><?php echo $no++; ?></td>
                                        <td><?php echo $data['id_pelanggan']; ?></td>
                                        <td><?php echo $data['nama_pelanggan']; ?></td>
                                        <td><?php echo $data['nomor_kwh']; ?></td>
                                        <td><?php echo $data['alamat']; ?></td>
                                        
                                       
                                        
                                            <td>

                                            <a href="hapus.php?id_pelanggan=<?php echo $data['id_pelanggan']; ?>"><button type="button" class="btn btn-danger"> <span class="glyphicon glyphicon-trash"></span> Hapus</button></a>

                                          </td>             </tr>

                                        <?php
                                      }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                            </div>
                    </div>
                    <!--End Advanced Tables -->
                </div>
            </div>
               
        </div>
               
    </div>
             <!-- /. PAGE INNER  -->
            </div>

         <!-- /. PAGE WRAPPER  -->
     <!-- /. WRAPPER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <script src="../assets/js/jquery-1.10.2.js"></script>
      <!-- BOOTSTRAP SCRIPTS -->
    <script src="../assets/js/bootstrap.min.js"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="../assets/js/jquery.metisMenu.js"></script>
     <!-- DATA TABLE SCRIPTS -->
    <script src="../assets/js/dataTables/jquery.dataTables.js"></script>
    <script src="../assets/js/dataTables/dataTables.bootstrap.js"></script>
        <script>
            $(document).ready(function () {
                $('#dataTables-example').dataTable();
            });
    </script>
         <!-- CUSTOM SCRIPTS -->
    <script src="../assets/js/custom.js"></script>
    
   
</body>
</html>
