<?php 
  include 'koneksi.php';
 session_start();
  if (!isset($_SESSION['username'])) {
    header('location:../login.php');
  }
  else{
    $query_admin = mysqli_query($koneksi, "SELECT * FROM admin where username='$_SESSION[username]'");
    $admin = mysqli_fetch_array($query_admin);
  }
  ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
      <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Aplikasi Pembayaran Lisrik</title>
    <link href="../assets/css/bootstrap.css" rel="stylesheet" />
    <link href="../assets/css/font-awesome.css" rel="stylesheet" />
    <link href="../assets/css/custom.css" rel="stylesheet" />
   <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
    <link href="../assets/js/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
</head>
<body>
    <div id="wrapper">
        <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand">I-PLN</a> 
            </div>
  <div style="color: white;
padding: 15px 50px 5px 50px;
float: right;
font-size: 16px;"><a href="../logout.php" class="btn btn-danger square-btn-adjust">Logout</a> </div>
        </nav>   
            <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">

        <li class="text-center">

                    <img src="../assets/img/7.png" class="user-image img-responsive"/>  
                    <h3 style="color: white;">Hallo Admin !!</h3>
                    <li>
                        <a href="#"><i class="fa fa-sitemap fa-2x"></i> Master Data<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="tarif.php">tarif</a>
                                <a href="index.php">Data Pengguna</a>
                            </li>
                           
                        </ul>
                      </li>  
                  <li  >
                    </li>
                    </li>

             
                    <li>
                        <a href="#"><i class="fa fa-credit-card fa-2x"></i>Laporan <span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="riwayat_pembayaran.php">Riwayat Pembayaran</a>
                                <a href="riwayat_tagihan.php">Riwayat Tagihan</a>
                                <a href="top_up.php">Riwayat TOP-UP</a>
                            </li>
                           
                        </ul>
                      </li>
                    <li>
                        <a  href="verifikasi.php"><i class="fa fa-dashboard fa-2x"></i> Verifikasi</a>
                    </li>
                     
                    <li>
                     <a  href="backup.php"><i class="fa fa-download fa-2x"></i> Backup  </a>
                    </li>           
                </ul>
               
            </div>
            
        </nav>  
        <!-- /. NAV SIDE  -->



        <div id="page-wrapper" >
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                     <h2 align="center"></h2>  
                    </div>
                </div>
                 <!-- /. ROW  -->
                 <hr />
               <!-- Button trigger modal -->

<a href="print_riwayat.php" class="btn btn-primary">
  Print To Pdf</a>
<!-- Modal -->

<br>
<br>
      
            <div class="row">
                <div class="col-md-12">
                   
                    <div class="panel panel-default">
                        <div class="panel-body">
                          <h4 align="center">Riwayat Tagihan</h4>
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>ID Pelanggan</th>
                                            <th>Bulan </th>
                                            <th>tahun</th>
                                            <th>Jumlah Meter</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                     <?php

                  include '../koneksi.php';
                  $query_tagihan = mysqli_query($koneksi, "select * from tagihan");
                  while($tagihan = mysqli_fetch_array($query_tagihan)){
                    $id_tagihan = $tagihan['id_tagihan'];
                                    $id_pelanggan = $tagihan['id_pelanggan'];
                $query_penggunaan = mysqli_query($koneksi, "SELECT * FROM penggunaan WHERE id_pelanggan='$id_pelanggan' and bulan='$tagihan[bulan]'");
                $penggunaan = mysqli_fetch_array($query_penggunaan);
                $id_penggunaan = $penggunaan['id_penggunaan'];

//Penggambilan data di tabel pelanggan
                $query_pelanggan = mysqli_query($koneksi, "SELECT * FROM pelanggan WHERE id_pelanggan='$id_pelanggan'");
                $pelanggan = mysqli_fetch_array($query_pelanggan);
                $nama_pelanggan = $pelanggan['nama_pelanggan'];
                $id_tarif = $pelanggan['id_tarif'];

//pengambilan data di tabel tarif berdasarkan id tarif pelanggan
                $query_tarif = mysqli_query($koneksi, "SELECT * FROM tarif WHERE id_tarif='$id_tarif'");
                $tarif = mysqli_fetch_array($query_tarif);
                $tarifperkwh = $tarif['tarifperkwh'];

                $bulan_tagihan = $penggunaan['bulan'];
                $tahun_tagihan = $penggunaan['tahun'];
                $jumlah_meter_penggunaan = $penggunaan['meter_akhir'] - $penggunaan['meter_awal'];
                $jumlah_tagihan = $jumlah_meter_penggunaan * $tarifperkwh;
                $tahun_cek = date("Y");
                $bulan_cek = date('n');
                $tanggal_denda = date('d');
                if ($tanggal_denda > 20){
                  $jumlah_denda = 5000;
                }else {
                  $jumlah_denda = 0;
                }
                    ?>
                    <tr>
                      <td><?php echo $tagihan['id_tagihan']; ?></td>
                      <?php
                      $query_user = mysqli_query($koneksi, "SELECT * FROM pelanggan WHERE id_pelanggan='$tagihan[id_pelanggan]'");
                      $user = mysqli_fetch_array($query_user);
                      ?>
                      <td><?php echo $user['nama_pelanggan']; ?></td>
                      <td>
                        <?php
                        switch ($tagihan['bulan']) {
                          case "1";
                          $bulan = "Januari";
                          break;
                          case "2";
                          $bulan = "Februari";
                          break;
                          case "3";
                          $bulan = "Maret";
                          break;
                          case "4";
                          $bulan = "April";
                          break;
                          case "5";
                          $bulan = "Mei";
                          break;
                          case "6";
                          $bulan = "Juni";
                          break;
                          case "7";
                          $bulan = "Juli";
                          break;
                          case "8";
                          $bulan = "Agustus";
                          break;
                          case "9";
                          $bulan = "September";
                          break;
                          case "10";
                          $bulan = "Oktober";
                          break;
                          case "11";
                          $bulan = "November";
                          break;
                          case "12";
                          $bulan = "Desember";
                          break;
                        }
                        echo $bulan; ?>
                      </td>
                      <td><?php echo $tagihan['tahun']; ?></td>
                      <td><?php echo $tagihan['jumlah_meter']; ?></td>
                      <td><?php echo $tagihan['status']; ?></td>
                      <td>
                        <a href=""><button type="button" class="btn btn-danger"> <span class="glyphicon glyphicon-trash"></span> Hapus</button></a>
                        
                        <button data-toggle="modal" data-target="#bayar<?php echo $id_tagihan;?>" class="btn btn-info btn">Verifikasi</button>
                      </td>
                    </tr>
                <!-- Modal -->
                <div class="modal fade" id="bayar<?php echo $id_tagihan;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Riwayat Tagihan yang belum Dibayar</h4>
                      </div>
                      <div class="modal-body">
                        <form action="pembayaran.php?p=<?php echo $id_tagihan; ?>" method="POST">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Nama Pelanggan</label>
                            <div class="col-sm-9">
                              <input type="text" name="nama_pelanggan" class="form-control" placeholder="Nama Pelanggan" value="<?php echo $pelanggan['nama_pelanggan']; ?>" readonly>
                            </div>
                          </div>
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Nomor KWH</label>
                            <div class="col-sm-9">
                              <input type="text" name="nomor_kwh" class="form-control" value="<?php echo $pelanggan['nomor_kwh']; ?>" placeholder="Nomor kwh" readonly>
                            </div>
                          </div>
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Daya (Watt)</label>
                            <div class="col-sm-9">
                              <input type="number" name="daya" class="form-control" placeholder="Daya" value="<?php echo $tarif['daya']; ?>" readonly>
                            </div>
                          </div>
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Tarif Per-Kwh</label>
                            <div class="col-sm-9">
                              <input type="number" name="tarifperkwh" class="form-control" placeholder="Bulan Bayar" value="<?php echo $tarif['tarifperkwh']; ?>" readonly>
                            </div>
                          </div>
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Jumlah Penggunaan</label>
                            <div class="col-sm-9">
                              <input type="number" name="jumlah_penggunaan" class="form-control" placeholder="Jumlah Bayar" value="<?php $jumlah = $penggunaan['meter_akhir'] - $penggunaan['meter_awal']; echo $jumlah; ?>" readonly>
                            </div>
                          </div>
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Jumlah Tagihan</label>
                            <div class="col-sm-9">
                              <input type="text" name="jumlah_tagihan" class="form-control"  placeholder="Total Bayar" value="<?php echo $jumlah_tagihan; ?>" readonly>
                            </div>
                          </div>
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Biaya Admin</label>
                            <div class="col-sm-9">
                              <input type="text" name="biaya_admin" class="form-control"  placeholder="Total Bayar" value="2500" readonly>
                            </div>
                          </div>
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Denda</label>
                            <div class="col-sm-9">
                              <input type="text" name="denda" class="form-control"  placeholder="Total Bayar" value="<?php echo $jumlah_denda; ?>" readonly>
                            </div>
                          </div>
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Total Bayar</label>
                            <div class="col-sm-9">
                              <input type="text" name="denda" class="form-control"  placeholder="Total Bayar" value="<?php $jumlah_bayar=$jumlah_tagihan + 2500 + $jumlah_denda; echo $jumlah_bayar; ?>" readonly>
                            </div>
                          </div>
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Status</label>
                            <div class="col-sm-9">
                              <input type="text" name="status" class="form-control"  placeholder="Total Bayar" value="<?php echo $tagihan['status']; ?>" readonly>
                            </div>
                          </div>
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Metode Pembayaran</label>
                            <div class="col-sm-9">
                              <select class="form-control" name="metode_pembayaran">
                                <option value="BRI">Bank BRI</option>
                                <option value="Mandiri">Bank Mandiri</option>
                                <option value="BNI">Bank BNI</option>
                              </select>
                            </div>
                          </div>
                        </div>
                        <div class="modal-footer">
                          <input type="submit" class="btn btn-danger" name="verifikasi" value="verifikasi">
                          <button type="button" class="btn btn-info" data-dismiss="modal">Close</button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
                  <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                            </div>
                    </div>
                    <!--End Advanced Tables -->
                </div>
            </div>      
               
        </div>
               
    </div>
             <!-- /. PAGE INNER  -->
            </div>

         <!-- /. PAGE WRAPPER  -->
     <!-- /. WRAPPER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <script src="../assets/js/jquery-1.10.2.js"></script>
      <!-- BOOTSTRAP SCRIPTS -->
    <script src="../assets/js/bootstrap.min.js"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="../assets/js/jquery.metisMenu.js"></script>
     <!-- DATA TABLE SCRIPTS -->
    <script src="../assets/js/dataTables/jquery.dataTables.js"></script>
    <script src="../assets/js/dataTables/dataTables.bootstrap.js"></script>
        <script>
            $(document).ready(function () {
                $('#dataTables-example').dataTable();
            });
    </script>
         <!-- CUSTOM SCRIPTS -->
    <script src="../assets/js/custom.js"></script>
    
   
</body>
</html>
