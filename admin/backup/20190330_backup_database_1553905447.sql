DROP TABLE admin;

CREATE TABLE `admin` (
  `id_admin` char(16) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(20) NOT NULL,
  `email` varchar(40) NOT NULL,
  `id_level` char(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO admin VALUES("1","indah","indah","indahf945@gmail.com","1");



DROP TABLE level;

CREATE TABLE `level` (
  `id_level` int(25) NOT NULL AUTO_INCREMENT,
  `nama_level` varchar(25) NOT NULL,
  PRIMARY KEY (`id_level`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




DROP TABLE pelanggan;

CREATE TABLE `pelanggan` (
  `id_pelanggan` varchar(25) NOT NULL,
  `username` varchar(15) NOT NULL,
  `password` varchar(50) NOT NULL,
  `nomor_kwh` varchar(20) NOT NULL,
  `nama_pelanggan` varchar(50) NOT NULL,
  `alamat` text NOT NULL,
  `saldo` int(11) NOT NULL,
  `id_tarif` int(25) NOT NULL,
  PRIMARY KEY (`id_pelanggan`),
  KEY `id_tarif` (`id_tarif`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO pelanggan VALUES("20190307001","puspita","12d2f20718abf7394dc4acff0ac19a0b","173672","Puspita Wulandari","Bogor","0","1");
INSERT INTO pelanggan VALUES("20190308002","nurul","6968a2c57c3a4fee8fadc79a80355e4d","1453566","Siti Nurul Laela","Bogor","0","2");
INSERT INTO pelanggan VALUES("27862723","indah","f3385c508ce54d577fd205a1b2ecdfb7","9286382","indah fitria","sindang barang","20000","1");



DROP TABLE pembayaran;

CREATE TABLE `pembayaran` (
  `id_pembayaran` varchar(25) NOT NULL,
  `id_tagihan` varchar(25) NOT NULL,
  `id_pelanggan` varchar(25) NOT NULL,
  `tanggal_pembayaran` datetime NOT NULL,
  `bulan_bayar` varchar(10) NOT NULL,
  `jumlah_bayar` int(11) NOT NULL,
  `biaya_admin` int(15) NOT NULL,
  `total_bayar` int(25) NOT NULL,
  `id_admin` char(15) NOT NULL,
  PRIMARY KEY (`id_pembayaran`),
  KEY `id_tagihan` (`id_tagihan`,`id_pelanggan`,`id_admin`),
  KEY `id_pelanggan` (`id_pelanggan`),
  KEY `id_admin` (`id_admin`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO pembayaran VALUES("20190329001","20190329001","27862723","2019-03-29 13:20:19","3","135000","2500","137500","indah");
INSERT INTO pembayaran VALUES("20190329002","","","2019-03-29 13:59:03","3","135000","2500","137500","indah");
INSERT INTO pembayaran VALUES("20190329003","20190329003","20190308002","2019-03-29 14:12:27","3","100000","2500","102500","indah");



DROP TABLE penggunaan;

CREATE TABLE `penggunaan` (
  `id_penggunaan` varchar(25) NOT NULL,
  `id_pelanggan` varchar(25) NOT NULL,
  `bulan` varchar(10) NOT NULL,
  `tahun` year(4) NOT NULL,
  `meter_awal` varchar(50) NOT NULL,
  `meter_akhir` varchar(50) NOT NULL,
  PRIMARY KEY (`id_penggunaan`),
  KEY `id_pelanggan` (`id_pelanggan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO penggunaan VALUES("20190329001","27862723","3","2019","0","100");
INSERT INTO penggunaan VALUES("20190329002","20190307001","3","2019","0","100");
INSERT INTO penggunaan VALUES("20190329003","20190308002","3","2019","0","100");



DROP TABLE saldo;

CREATE TABLE `saldo` (
  `id_saldo` varchar(12) NOT NULL,
  `username` varchar(20) NOT NULL,
  `jumlah_isi` int(10) NOT NULL,
  `metode` varchar(30) NOT NULL,
  `tanggal_pengisian` datetime NOT NULL,
  `status` varchar(40) NOT NULL,
  PRIMARY KEY (`id_saldo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO saldo VALUES("20190329014","indah","50000","Bank BRI","2019-03-29 13:41:00","Menunggu Verifikasi");
INSERT INTO saldo VALUES("20190329015","indah","10000","Bank BRI","2019-03-29 13:41:41","Menunggu Verifikasi");
INSERT INTO saldo VALUES("20190329016","indah","0","","2019-03-29 13:41:45","Menunggu Verifikasi");
INSERT INTO saldo VALUES("20190329017","indah","50000","Bank BRI","2019-03-29 13:44:09","Menunggu Verifikasi");
INSERT INTO saldo VALUES("20190329018","puspita","500000","Bank BRI","2019-03-29 13:49:09","Menunggu Verifikasi");
INSERT INTO saldo VALUES("20190329019","puspita","100","Bank BRI","2019-03-29 13:59:49","Menunggu Verifikasi");
INSERT INTO saldo VALUES("20190329020","nurul","500000","Bank BRI","2019-03-29 14:12:41","Menunggu Verifikasi");
INSERT INTO saldo VALUES("20190329021","indah","100","Bank BRI","2019-03-29 20:19:31","Menunggu Verifikasi");
INSERT INTO saldo VALUES("20190329022","indah","50","Bank BRI","2019-03-29 20:44:24","Menunggu Verifikasi");
INSERT INTO saldo VALUES("20190329023","indah","200","Bank BRI","2019-03-29 20:45:03","Menunggu Verifikasi");
INSERT INTO saldo VALUES("20190329024","nurul","100","Bank BRI","2019-03-29 20:46:08","Menunggu Verifikasi");
INSERT INTO saldo VALUES("20190330001","indah","100000","Bank BRI","2019-03-30 07:20:04","Menunggu Verifikasi");



DROP TABLE tagihan;

CREATE TABLE `tagihan` (
  `id_tagihan` varchar(12) NOT NULL,
  `id_penggunaan` varchar(12) NOT NULL,
  `id_pelanggan` varchar(12) NOT NULL,
  `bulan` varchar(2) NOT NULL,
  `tahun` year(4) NOT NULL,
  `jumlah_meter` varchar(25) NOT NULL,
  `status` varchar(15) NOT NULL,
  PRIMARY KEY (`id_tagihan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO tagihan VALUES("20190329001","20190329001","27862723","3","2019","100","Lunas");
INSERT INTO tagihan VALUES("20190329002","20190329002","20190307001","3","2019","100","Belum Dibayar");
INSERT INTO tagihan VALUES("20190329003","20190329003","20190308002","3","2019","100","Lunas");



DROP TABLE tarif;

CREATE TABLE `tarif` (
  `id_tarif` int(2) NOT NULL,
  `daya` varchar(25) NOT NULL,
  `tarifperkwh` int(6) NOT NULL,
  PRIMARY KEY (`id_tarif`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO tarif VALUES("1","900","1350");
INSERT INTO tarif VALUES("2","450","1000");
INSERT INTO tarif VALUES("3","1300","2000");



