DROP TABLE admin;

CREATE TABLE `admin` (
  `id_admin` char(16) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(20) NOT NULL,
  `email` varchar(40) NOT NULL,
  `id_level` char(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO admin VALUES("1","indah","kwhl1sbf","indahf945@gmail.com","1");
INSERT INTO admin VALUES("2","rohmah","7364e8572d4b517c30a7","strhmh811@gmail.com","2");



DROP TABLE level;

CREATE TABLE `level` (
  `id_level` int(25) NOT NULL AUTO_INCREMENT,
  `nama_level` varchar(25) NOT NULL,
  PRIMARY KEY (`id_level`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




DROP TABLE pelanggan;

CREATE TABLE `pelanggan` (
  `id_pelanggan` varchar(25) NOT NULL,
  `username` varchar(15) NOT NULL,
  `password` varchar(50) NOT NULL,
  `nomor_kwh` varchar(20) NOT NULL,
  `nama_pelanggan` varchar(50) NOT NULL,
  `alamat` text NOT NULL,
  `saldo` int(11) NOT NULL,
  `id_tarif` int(25) NOT NULL,
  PRIMARY KEY (`id_pelanggan`),
  KEY `id_tarif` (`id_tarif`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO pelanggan VALUES("20190307001","puspita","12d2f20718abf7394dc4acff0ac19a0b","173672","Puspita Wulandari","Bogor","97500","1");
INSERT INTO pelanggan VALUES("20190308002","nurul","6968a2c57c3a4fee8fadc79a80355e4d","1453566","Siti Nurul Laela","bogor","30000","2");
INSERT INTO pelanggan VALUES("27862723","indah","f3385c508ce54d577fd205a1b2ecdfb7","9286382","indah fitria","sindang barang jero","100000","2");



DROP TABLE pembayaran;

CREATE TABLE `pembayaran` (
  `id_pembayaran` varchar(25) NOT NULL,
  `id_tagihan` varchar(25) NOT NULL,
  `id_pelanggan` varchar(25) NOT NULL,
  `tanggal_pembayaran` datetime NOT NULL,
  `bulan_bayar` varchar(10) NOT NULL,
  `jumlah_bayar` int(11) NOT NULL,
  `biaya_admin` int(15) NOT NULL,
  `denda` int(11) NOT NULL,
  `total_bayar` int(25) NOT NULL,
  `id_admin` char(15) NOT NULL,
  PRIMARY KEY (`id_pembayaran`),
  KEY `id_tagihan` (`id_tagihan`,`id_pelanggan`,`id_admin`),
  KEY `id_pelanggan` (`id_pelanggan`),
  KEY `id_admin` (`id_admin`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO pembayaran VALUES("20190529001","20190529001","27862723","2019-05-29 07:25:24","5","10000","2500","5000","12500","indah");
INSERT INTO pembayaran VALUES("20190529002","20190529002","20190308002","2019-05-29 07:44:55","5","20000","2500","5000","22500","indah");
INSERT INTO pembayaran VALUES("20190701001","20190701001","20190308002","2019-07-01 08:01:26","7","20000","2500","0","22500","indah");



DROP TABLE penggunaan;

CREATE TABLE `penggunaan` (
  `id_penggunaan` varchar(25) NOT NULL,
  `id_pelanggan` varchar(25) NOT NULL,
  `bulan` varchar(10) NOT NULL,
  `tahun` year(4) NOT NULL,
  `meter_awal` varchar(50) NOT NULL,
  `meter_akhir` varchar(50) NOT NULL,
  PRIMARY KEY (`id_penggunaan`),
  KEY `id_pelanggan` (`id_pelanggan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO penggunaan VALUES("20190529001","27862723","5","2019","0","10");
INSERT INTO penggunaan VALUES("20190529002","20190308002","5","2019","0","20");
INSERT INTO penggunaan VALUES("20190601001","20190308002","6","2019","20","62");
INSERT INTO penggunaan VALUES("20190701001","20190308002","7","2019","62","82");



DROP TABLE saldo;

CREATE TABLE `saldo` (
  `id_saldo` varchar(12) NOT NULL,
  `username` varchar(20) NOT NULL,
  `jumlah_isi` int(10) NOT NULL,
  `metode` varchar(30) NOT NULL,
  `tanggal_pengisian` datetime NOT NULL,
  `status` varchar(40) NOT NULL,
  PRIMARY KEY (`id_saldo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO saldo VALUES("20190528001","indah","50000","Bank BRI","2019-05-29 01:50:35","Telah di verifikasi");
INSERT INTO saldo VALUES("20190601001","nurul","10000","Bank BRI","2019-06-01 07:48:10","Telah di verifikasi");
INSERT INTO saldo VALUES("20191201001","indah","100000","Bank BRI","2019-12-01 23:40:45","Telah di verifikasi");



DROP TABLE tagihan;

CREATE TABLE `tagihan` (
  `id_tagihan` varchar(12) NOT NULL,
  `id_penggunaan` varchar(12) NOT NULL,
  `id_pelanggan` varchar(12) NOT NULL,
  `bulan` varchar(2) NOT NULL,
  `tahun` year(4) NOT NULL,
  `jumlah_meter` varchar(25) NOT NULL,
  `status` varchar(15) NOT NULL,
  PRIMARY KEY (`id_tagihan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO tagihan VALUES("20190529001","20190529001","27862723","5","2019","10","Lunas");
INSERT INTO tagihan VALUES("20190529002","20190529002","20190308002","5","2019","20","Lunas");
INSERT INTO tagihan VALUES("20190601001","20190601001","20190308002","6","2019","42","Belum Dibayar");
INSERT INTO tagihan VALUES("20190701001","20190701001","20190308002","7","2019","20","Lunas");
INSERT INTO tagihan VALUES("20190701002","20190701001","20190308002","7","2019","20","Belum Dibayar");



DROP TABLE tarif;

CREATE TABLE `tarif` (
  `id_tarif` int(2) NOT NULL,
  `daya` varchar(25) NOT NULL,
  `tarifperkwh` int(6) NOT NULL,
  PRIMARY KEY (`id_tarif`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO tarif VALUES("1","900","1350");
INSERT INTO tarif VALUES("2","450","1000");
INSERT INTO tarif VALUES("3","13000","2000");



