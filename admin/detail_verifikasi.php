<?php 
  include 'koneksi.php';
 session_start();
  if (!isset($_SESSION['username'])) {
    header('location:../login.php');
  }
  else{
    $query_admin = mysqli_query($koneksi, "SELECT * FROM admin where username='$_SESSION[username]'");
    $admin = mysqli_fetch_array($query_admin);
  }
  ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
      <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Aplikasi Pembayaran Lisrik</title>
    <link href="../assets/css/bootstrap.css" rel="stylesheet" />
    <link href="../assets/css/font-awesome.css" rel="stylesheet" />
    <link href="../assets/css/custom.css" rel="stylesheet" />
   <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
    <link href="../assets/js/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
</head>
<body>
    <div id="wrapper">
        <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand">I-PLN</a> 
            </div>
  <div style="color: white;
padding: 15px 50px 5px 50px;
float: right;
font-size: 16px;"><a href="../logout.php" class="btn btn-danger square-btn-adjust">Logout</a> </div>
        </nav>   
           <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">

				<li class="text-center">

                    <img src="../assets/img/7.png" class="user-image img-responsive"/>	
                    <h3 style="color: white;">Hallo Admin !!</h3>
                    <li>
                        <a href="#"><i class="fa fa-sitemap fa-2x"></i> Master Data<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="tarif.php">tarif</a>
                                <a href="index.php">Data Pengguna</a>
                            </li>
                           
                        </ul>
                      </li>  
                  <li  >
                    </li>
                    </li>

						 
                     <li>
                        <a href="#"><i class="fa fa-credit-card fa-2x"></i> Laporan <span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="riwayat_pembayaran.php">Riwayat Pembayaran</a>
                                <a href="riwayat_tagihan.php">Riwayat Tagihan</a>
                                <a href="top_up.php">Riwayat TOP-UP</a>
                            </li>
                           
                        </ul>
                      </li>
                    <li>
                        <a  href="verifikasi.php"><i class="fa fa-dashboard fa-2x"></i> Verifikasi</a>
                    </li>
                    
                    <li>
                     <a  href="backup.php"><i class="fa fa-download fa-2x"></i> Backup  </a>
                    </li>   				
                </ul>
               
            </div>
            
        </nav>  
        <!-- /. NAV SIDE  -->

<?php
    $id_saldo = $_GET['id_saldo'];
    $pilih = mysqli_query($koneksi, "SELECT * FROM saldo WHERE id_saldo='$id_saldo'");
    $data = mysqli_fetch_array($pilih);
    ?>

        <div id="page-wrapper" >
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                     <h4 align="center">Verifikasi</h4>  
                    </div>
                </div>
                 <!-- /. ROW  -->
                 <hr />
             
        <div class="main-content">
            
 <div class="row">
        <div class="col-md-12">
          <!-- Form Elements -->
          <div class="panel panel-default">
            
            <div class="panel-body">
              <div class="row">
                <div class="col-md-8">
                  <form action="" method="POST">
                      <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Username</label>
                        <div class="col-sm-9">
                          <input type="text" name="username" class="form-control" value="<?php echo $data['username']; ?>" readonly>
                        </div>
                      </div>
                      <?php
                      $query_user = mysqli_query($koneksi, "SELECT * FROM pelanggan WHERE username='$data[username]'");
                      $user = mysqli_fetch_array($query_user);
                      ?>
                      <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Nama Pelanggan</label>
                        <div class="col-sm-9">
                          <input type="text" name="nama_pelanggan" class="form-control" value="<?php echo $user['nama_pelanggan']; ?>" readonly>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Saldo</label>
                        <div class="col-sm-9">
                          <input type="text" name="jumlah_isi" class="form-control" value="<?php echo $data['jumlah_isi']; ?>" readonly>
                        </div>
                      </div>
                      <button type="submit" class="btn btn-danger" name="verif">Verifikasi</button>
                    </form>
                </div>
                <?php
      if (isset($_POST['verif'])){
        $username = $_POST['username'];
        $nama_pelanggan = $_POST['nama_pelanggan'];
        $jumlah_isi = $_POST['jumlah_isi'];
if ($data['status']=="Telah di verifikasi") {
  echo "<script>window.alert('Status telah di verifikasi')
  window.location='verifikasi.php'</script>";
}else{
        $saldo = mysqli_query($koneksi,"UPDATE saldo SET username = '$username', jumlah_isi = '$jumlah_isi', status ='Telah di verifikasi' WHERE id_saldo='$id_saldo'");
        $pelanggan = mysqli_query($koneksi,"UPDATE pelanggan SET saldo = saldo+'$jumlah_isi' WHERE username='$data[username]'");
        if($saldo AND $pelanggan){
          echo "<script>window.alert('Data Berhasil Di Verifikasi')
          window.location='verifikasi.php'</script>";
        }else{
          echo "Gagal";
        }
      }
      } 
      ?>

              </div>
            </div>
          </div>
        </div>
      </div>

   </div>
           
               
        </div>
               
    </div>
             <!-- /. PAGE INNER  -->
            </div>

         <!-- /. PAGE WRAPPER  -->
     <!-- /. WRAPPER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <script src="../assets/js/jquery-1.10.2.js"></script>
      <!-- BOOTSTRAP SCRIPTS -->
    <script src="../assets/js/bootstrap.min.js"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="../assets/js/jquery.metisMenu.js"></script>
     <!-- DATA TABLE SCRIPTS -->
    <script src="../assets/js/dataTables/jquery.dataTables.js"></script>
    <script src="../assets/js/dataTables/dataTables.bootstrap.js"></script>
        <script>
            $(document).ready(function () {
                $('#dataTables-example').dataTable();
            });
    </script>
         <!-- CUSTOM SCRIPTS -->
    <script src="../assets/js/custom.js"></script>
    
   
</body>
</html>
