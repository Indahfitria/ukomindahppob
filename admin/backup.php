<?php
session_start();
include 'koneksi.php';
if(!isset($_SESSION['username'])) {
  header('location:../log.php');

}
?>

<?php
$connect = new PDO("mysql:host=localhost;dbname=ujikom_indah", "root", "");
$get_all_table_query = "SHOW TABLES";
$statement = $connect->prepare($get_all_table_query);
$statement->execute();
$result = $statement->fetchAll();

if(isset($_POST['table']))
{
 $output = '';
 foreach($_POST["table"] as $table)
 {
  $show_table_query = "SHOW CREATE TABLE " . $table . "";
  $statement = $connect->prepare($show_table_query);
  $statement->execute();
  $show_table_result = $statement->fetchAll();

  foreach($show_table_result as $show_table_row)
  {
   $output .= "\n\n" . $show_table_row["Create Table"] . ";\n\n";
  }
  $select_query = "SELECT * FROM " . $table . "";
  $statement = $connect->prepare($select_query);
  $statement->execute();
  $total_row = $statement->rowCount();

  for($count=0; $count<$total_row; $count++)
  {
   $single_result = $statement->fetch(PDO::FETCH_ASSOC);
   $table_column_array = array_keys($single_result);
   $table_value_array = array_values($single_result);
   $output .= "\nINSERT INTO $table (";
   $output .= "" . implode(", ", $table_column_array) . ") VALUES (";
   $output .= "'" . implode("','", $table_value_array) . "');\n";
  }
 }
 $file_name = 'ujikom_PLN_' . date('y-m-d') . '.sql';
 $file_handle = fopen($file_name, 'w+');
 fwrite($file_handle, $output);
 fclose($file_handle);
 header('Content-Description: File Transfer');
 header('Content-Type: application/octet-stream');
 header('Content-Disposition: attachment; filename=' . basename($file_name));
 header('Content-Transfer-Encoding: binary');
 header('Expires: 0');
 header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($file_name));
    ob_clean();
    flush();
    readfile($file_name);
    unlink($file_name);
}

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
      <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Aplikasi Pembayaran Lisrik</title>
    <link href="../assets/css/bootstrap.css" rel="stylesheet" />
    <link href="../assets/css/font-awesome.css" rel="stylesheet" />
    <link href="../assets/css/custom.css" rel="stylesheet" />
   <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
    <link href="../assets/js/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
</head>
<body>
    <div id="wrapper">
        <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand">I-PLN</a> 
            </div>
  <div style="color: white;
padding: 15px 50px 5px 50px;
float: right;
font-size: 16px;"><a href="../logout.php" class="btn btn-danger square-btn-adjust">Logout</a> </div>
        </nav>   
           </nav>   
           <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">

        <li class="text-center">

                    <img src="../assets/img/7.png" class="user-image img-responsive"/>  
                    <h3 style="color: white;">Hallo Admin !!</h3>
                    <li>
                        <a href="#"><i class="fa fa-sitemap fa-2x"></i> Master Data<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="tarif.php">tarif</a>
                                <a href="index.php">Data Pengguna</a>
                            </li>
                           
                        </ul>
                      </li>  
                  <li  >
                    </li>
                    </li>

             
                     <li>
                        <a href="#"><i class="fa fa-credit-card fa-2x"></i> Laporan <span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="riwayat_pembayaran.php">Riwayat Pembayaran</a>
                                <a href="riwayat_tagihan.php">Riwayat Tagihan</a>
                                <a href="top_up.php">Riwayat TOP-UP</a>
                            </li>
                           
                        </ul>
                      </li>
                    <li>
                        <a  href="verifikasi.php"><i class="fa fa-dashboard fa-2x"></i> Verifikasi</a>
                    </li>
                    
                    <li>
                     <a  href="backup.php"><i class="fa fa-download fa-2x"></i> Backup  </a>
                    </li>           
                </ul>
               
            </div>
            
        </nav>  
        <!-- /. NAV SIDE  -->



        <div id="page-wrapper" >
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                    
                    </div>
                </div>
                 <!-- /. ROW  -->
                 <hr />
               <!-- Button trigger modal -->
 <h2 align="center">Backup Database</h2>
    <br />
    <form method="post" id="export_form" >
     <h3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pilih Tabel Untuk Export</h3>
    <?php
    foreach($result as $table)
    {
    ?>
     <div class="checkbox">
      <label><input type="checkbox" class="checkbox_table" name="table[]" value="<?php echo $table["Tables_in_ujikom_indah"]; ?>" /> <?php echo $table["Tables_in_ujikom_indah"]; ?></label>
     </div>
    <?php
    }
    ?>
     <div class="form-group">
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="submit" name="submit" id="submit" class="btn btn-danger" value="Export" />
     </div>

      <h2>&nbsp;&nbsp;Backup Database To All</h2>
     <div class="form-group">
      <?php
                              error_reporting(0);
                              $file=date("Ymd").'_backup_database_'.time().'.sql';
                              backup_tables("localhost","root","","ujikom_indah",$file);
                            ?>
                                <div class="form-group pull-left">
                                  &nbsp;&nbsp;<a style="cursor:pointer" onclick="location.href='download_backup_data.php?nama_file=<?php echo $file;?>'" title="Download" class="btn btn-primary" >Backup</a>
                                </div> 
                                <?php
            /*
            untuk memanggil nama fungsi :: jika anda ingin membackup semua tabel yang ada didalam database, biarkan tanda BINTANG (*) pada variabel $tables = '*'
            jika hanya tabel-table tertentu, masukan nama table dipisahkan dengan tanda KOMA (,) 
            */
            function backup_tables($host,$user,$pass,$name,$nama_file,$tables ='*') {
              $link = mysql_connect($host,$user,$pass);
              mysql_select_db($name,$link);
              
              if($tables == '*'){
                $tables = array();
                $result = mysql_query('SHOW TABLES');
                while($row = mysql_fetch_row($result)){
                  $tables[] = $row[0];
                }
              }
              else{//jika hanya table-table tertentu
                $tables = is_array($tables) ? $tables : explode(',',$tables);
              }
              
              foreach($tables as $table){
                $result = mysql_query('SELECT * FROM '.$table);
                $num_fields = mysql_num_fields($result);
                                
                $return.= 'DROP TABLE '.$table.';';//menyisipkan query drop table untuk nanti hapus table yang lama
                $row2 = mysql_fetch_row(mysql_query('SHOW CREATE TABLE '.$table));
                $return.= "\n\n".$row2[1].";\n\n";
                
                for ($i = 0; $i < $num_fields; $i++) {
                  while($row = mysql_fetch_row($result)){
                    //menyisipkan query Insert. untuk nanti memasukan data yang lama ketable yang baru dibuat
                    $return.= 'INSERT INTO '.$table.' VALUES(';
                    for($j=0; $j<$num_fields; $j++) {
                      //akan menelusuri setiap baris query didalam
                      $row[$j] = addslashes($row[$j]);
                      $row[$j] = ereg_replace("\n","\\n",$row[$j]);
                      if (isset($row[$j])) { $return.= '"'.$row[$j].'"' ; } else { $return.= '""'; }
                      if ($j<($num_fields-1)) { $return.= ','; }
                    }
                    $return.= ");\n";
                  }
                }
                $return.="\n\n\n";
              }             
              //simpan file di folder
              $nama_file;
              
              $handle = fopen('backup/'.$nama_file,'w+');
              fwrite($handle,$return);
              fclose($handle);
            }
            ?>
      </div>
</form>
      </div>
      
    </div>
  </div>
</div>
            

         <!-- /. PAGE WRAPPER  -->
     <!-- /. WRAPPER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <script>
$(document).ready(function(){
 $('#submit').click(function(){
  var count = 0;
  $('.checkbox_table').each(function(){
   if($(this).is(':checked'))
   {
    count = count + 1;
   }
  });
  if(count > 0)
  {
   $('#export_form').submit();
  }
  else
  {
   alert("Please Select Atleast one table for Export");
   return false;
  }
 });
});
</script>

    <script src="../assets/js/jquery-1.10.2.js"></script>
      <!-- BOOTSTRAP SCRIPTS -->
    <script src="../assets/js/bootstrap.min.js"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="../assets/js/jquery.metisMenu.js"></script>
     <!-- DATA TABLE SCRIPTS -->
    <script src="../assets/js/dataTables/jquery.dataTables.js"></script>
    <script src="../assets/js/dataTables/dataTables.bootstrap.js"></script>
        <script>
            $(document).ready(function () {
                $('#dataTables-example').dataTable();
            });
    </script>
         <!-- CUSTOM SCRIPTS -->
    <script src="../assets/js/custom.js"></script>
    
   
</body>
</html>
