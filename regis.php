<?php
include 'koneksi.php';
?>
<!DOCTYPE html>
<html>
<head>
	<title>FORM REGISTRASI</title>
	<link rel="stylesheet" type="text/css" href="dist/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="lib/bootstrap/css/bootstrap.css">
		<link rel="stylesheet" type="text/css" href="assets/css/docs.min.css">
		<link rel="stylesheet" type="text/css" href="style.css">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
</head>
<body style="background-image: url(img/bk.jpg);">
	<br>
<br>
<br>
<div class="col-sm-6" style="margin-left: 25%;">
<div class="panel panel-default">
  <div class="panel-body">
			<br>
			<h4 style="text-align:center;">Registrasi</h4>
			<br>
  <form  action="simpanregis.php" method="POST">
   <div class="form-group row" style="margin-left: 5%;">

      <label  class="col-sm-4 col-form-label">Username</label>
      <div class="col-sm-6">
        <input type="text" name="username" class="form-control" id="username" placeholder="username" autocomplete="
        off" required="">
      </div>
    </div>
    <div class="form-group row" style="margin-left: 5%;">
      <label class="col-sm-4 col-form-label">Password</label>
      <div class="col-sm-6">
        <input type="password" name="password" class="form-control" id="password" placeholder="password" autocomplete="
        off" required="">
      </div>
    </div>
    <div class="form-group row" style="margin-left: 5%;">
      <label class="col-sm-4 col-form-label">Email</label>
      <div class="col-sm-6">
        <input type="email" name="email" class="form-control" id="email" placeholder="email" autocomplete="
        off" required="">
      </div>
    </div>
    <div class="form-group row" style="margin-left: 5%;">
      <label  class="col-sm-4 col-form-label">Nomor Kwh</label>
      <div class="col-sm-6">
        <input type="text" class="form-control"  name="nomor_kwh" id="nomor_kwh" placeholder="Nomor Kwh" autocomplete="
        off" required="">
      </div>
    </div>
    <div class="form-group row" style="margin-left: 5%;">
      <label class="col-sm-4 col-form-label">Nama Lengkap</label>
      <div class="col-sm-6">
        <input type="text" class="form-control" name="nama_pelanggan" id="nama_pelanggan" placeholder="Nama Lengkap" autocomplete="
        off" required="">
      </div>
    </div>
    <div class="form-group row" style="margin-left: 5%;">
      <label class="col-sm-4 col-form-label">Alamat</label>
      <div class="col-sm-6">
        <input type="text" class="form-control" name="alamat" placeholder="Alamat" autocomplete="
        off" required="">
      </div>
    </div>
    <div class="form-group" ">
      <label class="col-sm-4 col-form-label">Id Tarif </label>
      <div class="col-sm-6" style="margin-left: 5%;">
      <select class="form-control" name="id_tarif">
      <?php
      include 'koneksi.php';
      $query_tarif = mysqli_query ($koneksi,"SELECT * FROM tarif");
      while ($tarif = mysqli_fetch_array($query_tarif)) {?>
        <option value="<?php echo $tarif ['id_tarif'];?>"><?php echo $tarif ['daya'],"
        watt -",$tarif ['tarifperkwh'],"per kwh ";?></option>
        <?php 
      }
      ?>
      </select> 
    </div>
  </div>
    
    <br>
    <br>
    <br>
    <br>
    <div class="form-group row">
      <div class="col-sm-12">
        <button type="submit" class="btn btn-primary" name="simpan" style="margin-left: 65%;">SIGN IN</button>
      </div>
    </div>
  </form>
  </div>
</div>
</div>


<script>window.JQuery || document.write('<script src="assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
<script src="assets/js/docs.min.js"></script>
<script src="dist/js/bootstrap.min.js"></script>
<script src="dist/js/bootstrap.js"></script>
</body>
</html>